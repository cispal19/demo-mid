package com.midis.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.midis.dto.PersonaDTO;
import com.midis.dto.RespuestaApi;
import com.midis.model.Persona;
import com.midis.service.IPersonaService;



@RestController
@CrossOrigin
@RequestMapping(value = "api/persona")
public class PersonaController {
private static final Logger logger = LoggerFactory.getLogger(PersonaController.class);
	
	@Autowired
	private IPersonaService  personaService;
	
	
	@GetMapping(value = "listar", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Persona>> listar(){
		try {
			return new ResponseEntity<List<Persona>>(personaService.listar(),HttpStatus.OK);
		} catch (Exception e) {
			logger.error("Error: ",e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@PostMapping(value="registrar", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<RespuestaApi> registrar(@RequestBody PersonaDTO persona){
		try {
			Persona per = new Persona();
			per.setNombres(persona.getNombres());
			per.setApellidos(persona.getApellidos());
			per.setDni(persona.getDni());
			per.setSexo(persona.getSexo());
			personaService.registrar(per);
			return new ResponseEntity<RespuestaApi>(new RespuestaApi("OK",""),HttpStatus.OK);
		} catch (Exception e) {
			logger.error("Error: ",e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
	}
	
	@DeleteMapping(value="eliminar/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<RespuestaApi> eliminar(@PathVariable int id){
		try {
			personaService.eliminar(id);
			return new ResponseEntity<RespuestaApi>(new RespuestaApi("OK",""),HttpStatus.OK);
		} catch (Exception e) {
			logger.error("Error: ",e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
	}
}
