package com.midis.service.impl;

import java.io.Serializable;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.midis.dao.IPersonaDao;
import com.midis.model.Persona;
import com.midis.service.IPersonaService;

@Service
public class PersonaServiceImpl implements IPersonaService {
	
	@Autowired
	private IPersonaDao dao;

	@Override
	public Persona registrar(Persona t) {
		
		return dao.save(t);
		
	}

	@Override
	public Persona modificar(Persona t) {
		return dao.save(t);
		
	}

	@Override
	public void eliminar(int id) {
		dao.deleteById(id);;
		
	}

	@Override
	public Persona listarId(int id) {
		// TODO Auto-generated method stub
		return dao.findById(id).orElse(null);
	}

	@Override
	public List<Persona> listar() {
		
		return dao.findAll();
	}



}
