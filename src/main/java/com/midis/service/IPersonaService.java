package com.midis.service;

import java.util.List;

import com.midis.model.Persona;


public interface IPersonaService {
	
	public Persona registrar(Persona t);

	public Persona modificar(Persona t);

	public void eliminar(int id);

	public Persona listarId(int id);

	public List<Persona> listar();

}
