package com.midis.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.midis.model.Persona;


@Repository
public interface IPersonaDao extends JpaRepository<Persona, Integer> {

}
